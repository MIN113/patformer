﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheck : MonoBehaviour {

    private Player player;

	void Start () {
        player = gameObject.GetComponentInParent<Player>();
	}
	
	public void OnTriggerEnter2D (Collider2D other) {
        player.grounded = true;
	}

    public void OnTriggerExit2D(Collider2D other)
    {
        player.grounded = false;
    }


    public void OnTriggerStay2D(Collider2D other)
    {
        player.grounded = true;
    }
}
