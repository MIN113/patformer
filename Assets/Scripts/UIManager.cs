﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    private Text score;
    private Vector3 player;
    private float maxScore;

	void Start () {
        
    }


    void Update()
    {
        score = GameObject.Find("Score").GetComponent<Text>();
        player = GameObject.Find("Player").GetComponent<Transform>().transform.position;

        score.text = ("Score :") + player.y.ToString("F0").PadLeft(4, '0');

    }
}
