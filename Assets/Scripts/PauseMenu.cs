﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    private GameObject pauseHUD;
    private bool paused;

	void Start () {

        pauseHUD = GameObject.Find("PauseUI");
        pauseHUD.SetActive ( false );
	}


    void Update() {

        if (Input.GetButtonDown("Pause"))
        {
            paused = !paused;
        }

        if (paused) {
            pauseHUD.SetActive ( true );
            Time.timeScale = 0;
        }

        if (!paused) {
            pauseHUD.SetActive( false );
            Time.timeScale = 1;
        }
    }

    public void Resume() {
        paused = false;
    }

    public void Exit(){
        Application.Quit();
    }

    public void Restart()
    {
        SceneManager.LoadScene("GameScreen", LoadSceneMode.Single);

    }
    public void MainMenu()
    {
        SceneManager.LoadScene("MenuScreen", LoadSceneMode.Single);

    }

}
