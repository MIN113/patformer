﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour {

    private Vector2 velocity;

    public float smoothX;
    public float smoothY;

    private float x;
    private float y;

    private GameObject player;

	void Start () {
        player = GameObject.Find("Player");

    }
	
	void FixedUpdate () {

        x = Mathf.SmoothDamp(transform.position.x, player.transform.position.x, ref velocity.x, smoothX);
        y = Mathf.SmoothDamp(transform.position.y, player.transform.position.y, ref velocity.y, smoothY);

        transform.position = new Vector3(x, y, transform.position.z);
    }
}
