﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {

    public float speed = 50f;
    private float maxSpeed = 3;

    public float jumpPow = 150f;

    public bool grounded;

    private Rigidbody2D rb2d;
    private float h;

    public int curLives;
    public int maxLives;

    private Animator anim;
    public AudioSource jump;
    public AudioSource death;

	void Start () {
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        anim = GameObject.Find("Sprite").GetComponent<Animator>();

        AudioSource jump = GetComponent<AudioSource>();
        AudioSource death = GetComponent<AudioSource>();

        curLives = maxLives;
	}

    private void Update()
    {
        anim.SetBool("Grounded", grounded);
        anim.SetFloat("Speed", Mathf.Abs(rb2d.velocity.x));

        if (Input.GetAxis("Horizontal") > 0.1f)
        {
            transform.localScale = new Vector3(1, 1, 1);

        }

        if (Input.GetAxis("Horizontal") < -0.1f)
        {
            transform.localScale = new Vector3(-1, 1, 1);

        }

        if (Input.GetButtonDown("Jump") && grounded)
        {
            rb2d.AddForce (Vector2.up * jumpPow);
            jump.Play();
        }

        if (curLives > maxLives) {
            curLives = maxLives;
        }

        if (curLives <= 0) {
            curLives = 0;

            Damage(1);
        }


    }

    void FixedUpdate(){

        Vector3 easeVelocity = rb2d.velocity;
        easeVelocity.y = rb2d.velocity.y;
        easeVelocity.z = 0.0f;
        easeVelocity.x *= 0.75f;

        h = Input.GetAxis("Horizontal");
        rb2d.AddForce((Vector2.right * speed) * h);

        if (rb2d.velocity.x > maxSpeed)
        {
            rb2d.velocity = new Vector2 ( maxSpeed , rb2d.velocity.y);
        }

        if (rb2d.velocity.x < -maxSpeed)
        {
            rb2d.velocity = new Vector2( -maxSpeed, rb2d.velocity.y);
        }
    }

    void Die()
    {
        death.Play();

        if (death.isPlaying) {
            GetComponent<PauseMenu>().Restart();
        }
        
        
    }

    public void Damage(int dmg) {

        death.Play();
        if (death.isPlaying)
        {
            SceneManager.LoadScene("MainScreen", LoadSceneMode.Single);
            curLives -= dmg;
        }
    }
}


