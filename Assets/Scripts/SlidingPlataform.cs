﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlidingPlataform : MonoBehaviour {

    public Vector3 velocity;


    void OnCollisionEnter2D (Collision2D other)
    {
        if (other.gameObject.tag == ("Player"))
        {
            other.collider.transform.SetParent(transform);
        }
    }

    void OnCollisionExit2D (Collision2D other)
    {
        if (other.gameObject.tag == ("Player"))
        {
            other.transform.parent = null;
        }
    }

    private void FixedUpdate()
        { 
            transform.position += (velocity * Time.deltaTime);

        if (transform.position.x >= 3.98f) {
            velocity = velocity * -1;
        }

        if (transform.position.x <= -3.98f)
        {
            velocity = velocity * -1;
        }
    }
}
